const path = require("path");

exports.onCreateWebpackConfig = ({ actions, stage }) => {
  if (stage === "develop-html" || stage === "build-html") {
    actions.setWebpackConfig({
      resolve: {
        mainFields: ["main"],
      },
    });
  } else {
    actions.setWebpackConfig({
      resolve: {
        mainFields: ["browser", "module", "main"],
      },
    });
  }
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  const projectTemplate = path.resolve("src/templates/projectTemplate.js");

  return graphql(`
    {
      allProject {
        edges {
          node {
            id
          }
        }
      }
    }
  `).then((result) => {
    if (result.errors) {
      throw result.errors;
    }

    result.data.allProject.edges.forEach((project) => {
      createPage({
        path: `/project/${project.node.id}`,
        component: projectTemplate,
        context: { projectId: project.node.id },
      });
    });
  });
};
