import React, { useContext } from "react";
import { graphql } from "gatsby";
import { FirebaseContext } from "../components/Firebase";
import ProjectItem from "../components/ProjectItem";
import { ProjectComments } from "../components/common";

const ProjectTemplate = (props) => {
  const { firebase } = useContext(FirebaseContext);
  return (
    <section>
      <ProjectItem
        name={props.data.project.name}
        description={props.data.project.description}
        imageUrl={props.data.project.localImage.childImageSharp.fixed}
      />
      {!!firebase && (
        <ProjectComments
          firebase={firebase}
          projectId={props.data.project.id}
        />
      )}
    </section>
  );
};

export const query = graphql`
  query ProjectQuery($projectId: String!) {
    project(id: { eq: $projectId }) {
      name
      description
      localImage {
        childImageSharp {
          fixed(width: 200) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      id
    }
  }
`;

export default ProjectTemplate;
