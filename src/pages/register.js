import React, { useState, useContext } from "react";
import { navigate } from "gatsby";
import { FirebaseContext } from "../components/Firebase";
import { Form, Input, Button, ErrorMessage } from "../components/common";

const Register = () => {
  const { firebase } = useContext(FirebaseContext);
  const [errorMessage, setErrorMessage] = useState("");

  const [formValues, setFormValues] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    username: "",
  });

  function handleSubmit(e) {
    e.preventDefault();

    if (formValues.password === formValues.confirmPassword) {
      firebase
        .register({
          username: formValues.username,
          email: formValues.email,
          password: formValues.password,
        })
        .then(() => navigate("/"))
        .catch((error) => {
          setErrorMessage(error.message);
        });
    } else {
      setErrorMessage("Password and Confirm password fields must match.");
    }
  }

  function handleChanges(e) {
    e.persist();
    setFormValues((currentValues) => ({
      ...currentValues,
      [e.target.name]: e.target.value,
    }));
    setErrorMessage("");
  }

  return (
    <Form onSubmit={handleSubmit}>
      <h2>Register</h2>
      <Input
        value={formValues.username}
        name="username"
        onChange={handleChanges}
        type="text"
        placeholder="Username"
      />
      <Input
        value={formValues.email}
        name="email"
        onChange={handleChanges}
        type="email"
        placeholder="Email"
      />
      <Input
        value={formValues.password}
        name="password"
        onChange={handleChanges}
        type="password"
        placeholder="Password"
      />
      <Input
        value={formValues.confirmPassword}
        name="confirmPassword"
        onChange={handleChanges}
        type="password"
        placeholder="Confirm password"
      />
      <Button type="submit" block>
        Register
      </Button>
      {!!errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </Form>
  );
};

export default Register;
