import React from "react";

import { Form } from "../components/common/Form";
import { Input } from "../components/common/Input";
import { Button } from "../components/common/Button";
import { ErrorMessage } from "../components/common/ErrorMessage";
import { SuccessMessage } from "../components/common/SuccessMessage";
import { TextArea } from "../components/common/TextArea";

export default class ContactPage extends React.Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: "",
    };
  }

  render() {
    const { status } = this.state;
    return (
      <Form
        onSubmit={this.submitForm}
        action="https://formspree.io/f/mdoprega"
        method="POST"
      >
        <h2>Contact Me</h2>
        <Input type="email" name="email" placeholder="Email" />
        <Input type="number" name="phone" placeholder="Phone Number" />
        <TextArea type="text" name="message" placeholder="Message" />

        <Button type="submit" block>
          Submit
        </Button>
        {status === "ERROR" ? (
          <ErrorMessage>There was an error!</ErrorMessage>
        ) : (
          ""
        )}
        {status === "SUCCESS" ? (
          <SuccessMessage>Thank you!</SuccessMessage>
        ) : (
          ""
        )}
      </Form>
    );
  }

  submitForm(ev) {
    ev.preventDefault();
    const form = ev.target;
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}
