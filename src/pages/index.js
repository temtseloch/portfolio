import React from "react";
import { graphql } from "gatsby";

import AboutSection from "../components/AboutSection";
import SkillSection from "../components/SkillSection";
import ExperienceSection from "../components/ExperienceSection";
import ProjectSection from "../components/ProjectSection";
import Footer from "../components/footer";

const IndexPage = (props) => {
  console.log(props);

  return (
    <section>
      <AboutSection />
      <SkillSection data={props.data.allSkill.edges} />
      <ExperienceSection data={props.data.allExperience.edges} />
      <ProjectSection data={props.data.allProject.edges} />
      <Footer />
    </section>
  );
};

export const query = graphql`
  {
    allProject {
      edges {
        node {
          name
          description
          localImage {
            childImageSharp {
              fixed(width: 200) {
                ...GatsbyImageSharpFixed
              }
            }
          }
          id
        }
      }
    }
    allExperience {
      edges {
        node {
          company
          date
          description
          position
          id
        }
      }
    }
    allSkill {
      edges {
        node {
          name
          id
        }
      }
    }
  }
`;

export default IndexPage;
