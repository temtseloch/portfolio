import React from "react";
import AddSkill from "../components/AddSkill";
import AddExperience from "../components/AddExperience";
import AddProject from "../components/AddProject";

const EditProfile = () => (
  <section>
    <AddSkill />
    <AddExperience />
    <AddProject />
  </section>
);

export default EditProfile;
