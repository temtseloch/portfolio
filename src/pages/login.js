import React, { useState, useContext } from "react";
import { navigate } from "gatsby";
import { FirebaseContext } from "../components/Firebase";

import { Form, Input, Button, ErrorMessage } from "../components/common";

const Login = () => {
  const [formValues, setFormValues] = useState({ email: "", password: "" });
  const { firebase } = useContext(FirebaseContext);
  const [errorMessage, setErrorMessage] = useState("");

  function handleSubmit(e) {
    e.preventDefault();

    firebase
      .login({ email: formValues.email, password: formValues.password })
      .then(() => navigate("/"))
      .catch((error) => {
        console.log(error);
        setErrorMessage(error.message);
      });
  }

  function handleChanges(e) {
    e.persist();
    setFormValues((currentValues) => ({
      ...currentValues,
      [e.target.name]: e.target.value,
    }));
    setErrorMessage("");
  }

  return (
    <section>
      <Form onSubmit={handleSubmit}>
        <h2>Login</h2>
        <Input
          required
          value={formValues.email}
          name="email"
          onChange={handleChanges}
          type="email"
          placeholder="Email"
        />
        <Input
          required
          value={formValues.password}
          name="password"
          onChange={handleChanges}
          type="password"
          placeholder="Password"
        />
        <Button type="submit" block>
          Login
        </Button>
        {!!errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
      </Form>
    </section>
  );
};

export default Login;
