import React from "react";

import styled from "styled-components";

const Skills = styled.section`
  margin-top: 50px;

  h2 {
    text-align: center;
  }

  .skills {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }

  .skill {
    width: 140px;
    border: 1px solid #ddd;
    border-radius: 8px;
    padding: 10px;
    margin: 10px;
    background: #1dd3b0;
  }
`;

const SkillSection = ({ data }) => (
  <Skills>
    <h2>My Skills</h2>
    <div className="skills">
      {data.map((edge) => (
        <div className="skill" key={edge.node.id}>
          <span>{edge.node.name}</span>
        </div>
      ))}
    </div>
  </Skills>
);

export default SkillSection;
