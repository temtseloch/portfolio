import React, { useState, useContext } from "react";
import { navigate } from "gatsby";
import { FirebaseContext } from "./Firebase";
import { Form, Input, Button } from "./common";
import styled from "styled-components";

const FormField = styled.div`
  margin-bottom: 8px;
`;

const AddExperience = () => {
  const { firebase } = useContext(FirebaseContext);
  const [formValues, setFormValues] = useState({
    company: "",
    date: "",
    position: "",
    description: "",
  });

  function handleSubmit(e) {
    e.preventDefault();

    firebase
      .createExperience({
        company: formValues.company,
        date: formValues.date,
        position: formValues.position,
        description: formValues.description,
      })
      .then(() => navigate("/"));
  }

  function handleChanges(e) {
    e.persist();
    setFormValues((currentValues) => ({
      ...currentValues,
      [e.target.name]: e.target.value,
    }));
  }

  return (
    <Form
      style={{
        border: "1px solid #ddd",
        marginBottom: "50px",
        padding: "10px",
        borderRadius: "10px",
      }}
      onSubmit={handleSubmit}
    >
      <FormField>
        <Input
          value={formValues.company}
          name="company"
          placeholder="Company"
          type="text"
          onChange={handleChanges}
        />
      </FormField>
      <FormField>
        <Input
          value={formValues.date}
          name="date"
          placeholder="Date"
          type="text"
          onChange={handleChanges}
        />
      </FormField>
      <FormField>
        <Input
          value={formValues.position}
          name="position"
          placeholder="Position"
          type="text"
          onChange={handleChanges}
        />
      </FormField>
      <FormField>
        <Input
          value={formValues.description}
          name="description"
          placeholder="Description"
          type="text"
          onChange={handleChanges}
        />
      </FormField>

      <Button type="submit" block>
        Add experience
      </Button>
    </Form>
  );
};

export default AddExperience;
