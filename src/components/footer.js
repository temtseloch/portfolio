import React from "react";
import styled from "styled-components";

const FooterWrapper = styled.div`
  text-align: center;
  margin-top: 30px;
`;

const Footer = () => (
  <FooterWrapper>
    <hr />
    <span>Copyright &copy; 2021</span>
  </FooterWrapper>
);

export default Footer;
