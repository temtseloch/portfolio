import styled from "styled-components";
import React from "react";

import Img from "gatsby-image";

const ProjectItemWrapper = styled.section`
  border: 1px solid #ddd;
  padding: 8px;
  margin-bottom: 8px;
  display: flex;

  .image {
    width: 60%;
    margin-right: 10%;
  }

  @media screen and (max-width: 700px) {
    flex-direction: column;
    align-items: center;

    .image {
      margin: 0;
    }
  }
`;

const ProjectItem = ({ name, description, imageUrl, children }) => {
  return (
    <ProjectItemWrapper>
      <div className="image">
        <Img className="img" fixed={imageUrl} />
      </div>

      <div className="info">
        <h3>{name}</h3>
        <p>{description}</p>
        <div>{children}</div>
      </div>
    </ProjectItemWrapper>
  );
};

export default ProjectItem;
