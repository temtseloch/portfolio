import { Link, navigate } from "gatsby";
import React, { useContext } from "react";
import { FirebaseContext } from "./Firebase";
import styled from "styled-components";

const LogoutLink = styled.span`
  color: white;
  cursor: pointer;
  font-weight: 400;
  font-size: 17px;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  &:hover {
    text-decoration: underline;
  }
`;

const HeaderWrapper = styled.header`
  background: #2a9d8f;
  height: 80px;
  margin-bottom: 50px;

  @media only screen and (max-width: 600px) {
    h1 {
      font-size: 1.8rem;
    }
  }
`;

const HeaderContent = styled.div`
  margin: 0 auto;
  maxwidth: 960;
  padding: 1.45rem 1.0875rem;
  display: flex;
  align-items: center;
  height:80px;

  > h1 {
    margin: 0;
    flex-grow: 1;
    font-weight: 400;
    letter-spacing: 3px;

    > a {
      color: white;
      text-decoration: none;
      }
    }
  }

  > div {
    margin: auto 0;
  }

  
`;

const UserInfo = styled.div`
  text-align: right;
  color: #14213d;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  margin-top: -12px;

  a {
    color: white;
    cursor: pointer;
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`;

const LoginLink = styled.div`
  margin: auto 0;

  a {
    color: white;
    text-decoration: none;
    cursor: pointer;

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Divider = styled.span`
  margin: 0 8px;
  padding-right: 1px;
  background: #ddd;
`;

const AdminLink = styled.span`
  a {
    color: white;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Header = () => {
  const { firebase, user } = useContext(FirebaseContext);

  function handleLogout() {
    firebase.logout().then(() => navigate("/login"));
  }

  return (
    <HeaderWrapper>
      <HeaderContent>
        <h1>
          <Link to="/">TEMTSEL</Link>
        </h1>
        <div>
          {!!user && !!user.email && (
            <UserInfo>
              {(user.username || user.email).charAt(0).toUpperCase() +
                (user.username || user.email).slice(1)}
              <div>
                {!!user.isAdmin && (
                  <>
                    <AdminLink>
                      <Link to="/edit-profile">Edit Profile</Link>
                    </AdminLink>
                  </>
                )}
                {!user.isAdmin && <Link to="/contact">Contact</Link>}
                <Divider />
                <LogoutLink onClick={handleLogout}>Logout</LogoutLink>
              </div>
            </UserInfo>
          )}
          {(!user || !user.email) && (
            <LoginLink>
              <Link to="/contact">Contact</Link>
              <Divider />
              <Link to="/login">Login</Link>
              <Divider />
              <Link to="/register">Register</Link>
            </LoginLink>
          )}
        </div>
      </HeaderContent>
    </HeaderWrapper>
  );
};

export default Header;
