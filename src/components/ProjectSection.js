import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import Img from "gatsby-image";

const Projects = styled.section`
  margin-top: 30px;
  h2 {
    text-align: center;
  }
  .projects {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
  }

  .project {
    border: 1px solid #ddd;
    width: 250px;
    padding: 20px;
    margin: 20px;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
  }

  @media only screen and (max-width: 636px) {
    .projects {
      justify-content: center;
    }
  }
`;

const LinkButton = styled.div`
  margin-top: 20px;
  a {
    padding: 6px 16px;
    background: rebeccapurple;
    color: white;
    border-radius: 8px;
    text-decoration: none;

    &:hover {
      background: indigo;
    }
  }
`;

const ProjectSection = ({ data }) => (
  <Projects>
    <h2>My Projects</h2>
    <div className="projects">
      {data.map((edge) => (
        <div className="project" key={edge.node.id}>
          <h3>{edge.node.name}</h3>
          <Img fixed={edge.node.localImage.childImageSharp.fixed} />
          <LinkButton>
            <Link to={`/project/${edge.node.id}`}>See details</Link>
          </LinkButton>
        </div>
      ))}
    </div>
  </Projects>
);

export default ProjectSection;
