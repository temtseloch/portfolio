import firebaseConfig from "./config";
import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";

app.initializeApp(firebaseConfig);

class Firebase {
  constructor() {
    if (!firebaseInstance) {
      this.auth = app.auth();
      this.db = app.firestore();
      this.functions = app.functions();
      this.storage = app.storage();
    }
  }

  subscribeToProjectComments({ projectId, onSnapshot }) {
    const projectRef = this.db.collection("projects").doc(projectId);
    return this.db
      .collection("comments")
      .where("project", "==", projectRef)
      .orderBy("dateCreated", "desc")
      .onSnapshot(onSnapshot);
  }

  async postComment({ text, projectId }) {
    const postCommentCallable = this.functions.httpsCallable("postComment");
    return postCommentCallable({
      text,
      projectId,
    });
  }

  getUserProfile({ userId, onSnapshot }) {
    return this.db
      .collection("publicProfiles")
      .where("userId", "==", userId)
      .limit(1)
      .onSnapshot(onSnapshot);
  }

  async register({ email, password, username }) {
    await this.auth.createUserWithEmailAndPassword(email, password);
    const createProfileCallable = this.functions.httpsCallable(
      "createPublicProfile"
    );
    return createProfileCallable({ username });
  }

  async login({ email, password }) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }

  async logout() {
    await this.auth.signOut();
  }

  async createSkill({ skillName }) {
    const createSkillCallable = this.functions.httpsCallable("createSkill");

    return createSkillCallable({
      skillName,
    });
  }

  async createProject({ projectName, description, imageUrl }) {
    const createProjectCallable = this.functions.httpsCallable("createProject");

    return createProjectCallable({
      projectName,
      description,
      imageUrl,
    });
  }

  async createExperience({ company, date, position, description }) {
    const createExperienceCallable = this.functions.httpsCallable(
      "createExperience"
    );

    return createExperienceCallable({
      company,
      date,
      position,
      description,
    });
  }
}

let firebaseInstance;

function getFirebaseInstance() {
  if (!firebaseInstance) {
    firebaseInstance = new Firebase();
    return firebaseInstance;
  } else if (firebaseInstance) {
    return firebaseInstance;
  } else {
    return null;
  }
}

export default getFirebaseInstance;
