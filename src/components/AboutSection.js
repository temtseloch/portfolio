import React from "react";

import styled from "styled-components";

const About = styled.section`
  .background {
    background-image: url("https://images.unsplash.com/photo-1587620962725-abab7fe55159?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80");
    background-position: center;
    background-repeat: no-repeat;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-itemts: center;

    .image {
      width: 30%;
      margin: 5% 35% 7.5% 35%;

      img {
        border-radius: 100%;
      }
    }

    .text {
      height: 15vh;
      background: white;
      opacity: 0.8;

      p {
        text-align: center;
        margin-top: 2%;
      }
    }

    @media only screen and (max-width: 600px) {
      p {
        font-size: 14px;
      }

      .image {
        width: 40%;
        margin: 7.5% 30% 7.5% 30s%;
      }
    }
  }
`;

const AboutSection = () => (
  <About>
    <div className="background">
      <div className="image">
        <img
          alt="img"
          src="https://firebasestorage.googleapis.com/v0/b/portfolio-60ac5.appspot.com/o/image.JPG?alt=media&token=8b01f633-edcd-40ed-ad9e-4cb5e729e46e"
        />
      </div>
      <div className="text">
        <p>
          Hi, my name is Temtsel-Och Ulziisaikhan. I go by Temka. I am a
          full-stack web developer from Lexington, KY. I am looking to gain
          experience in a competitive workplace. Thank you for looking into my
          portfolio!
        </p>
      </div>
    </div>
  </About>
);

export default AboutSection;
