import React, { useState, useContext } from "react";
import { navigate } from "gatsby";
import { FirebaseContext } from "./Firebase";
import { Form, Input, Button } from "./common";
import styled from "styled-components";

const FormField = styled.div`
  margin-bottom: 8px;
`;

const AddProject = () => {
  const { firebase } = useContext(FirebaseContext);
  const [formValues, setFormValues] = useState({
    projectName: "",
    description: "",
    imageUrl: "",
  });

  function handleSubmit(e) {
    e.preventDefault();

    firebase
      .createProject({
        projectName: formValues.projectName,
        description: formValues.description,
        imageUrl: formValues.imageUrl,
      })
      .then(() => navigate("/"));
  }

  function handleChanges(e) {
    e.persist();
    setFormValues((currentValues) => ({
      ...currentValues,
      [e.target.name]: e.target.value,
    }));
  }

  return (
    <Form
      style={{
        border: "1px solid #ddd",
        marginBottom: "50px",
        padding: "10px",
        borderRadius: "10px",
      }}
      onSubmit={handleSubmit}
    >
      <FormField>
        <Input
          value={formValues.projectName}
          name="projectName"
          placeholder="Project Name"
          type="text"
          onChange={handleChanges}
        />
      </FormField>
      <FormField>
        <Input
          value={formValues.imageUrl}
          name="imageUrl"
          placeholder="Image URL"
          type="text"
          onChange={handleChanges}
        />
      </FormField>
      <FormField>
        <Input
          value={formValues.description}
          name="description"
          placeholder="Description"
          type="text"
          onChange={handleChanges}
        />
      </FormField>

      <Button type="submit" block>
        Add new project
      </Button>
    </Form>
  );
};

export default AddProject;
