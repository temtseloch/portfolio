import React from "react";

import styled from "styled-components";

const Experiences = styled.section`
  margin-top: 50px;
  h2 {
    text-align: center;
  }

  .experience {
    margin-top: 30px;
    display: flex;
    align-items: center;
    justify-content: center;

    .details {
      width: 30%;
      margin-right: 10%;

      .date {
        font-style: italic;
        font-size: 16px;
      }

      .position {
        font-weight: 500;
      }
    }

    .description {
      width: 100%;
    }
  }

  @media only screen and (max-width: 600px) {
    .experience {
      display: block;

      .details {
        width: 100%;
      }
    }
  }
`;

const ExperienceSection = ({ data }) => (
  <Experiences>
    <h2>My Experience</h2>
    {data.map((edge) => (
      <div className="experience" key={edge.node.id}>
        <div className="details">
          <h3>{edge.node.company}</h3>
          <p className="date">{edge.node.date}</p>
          <p className="position">{edge.node.position}</p>
        </div>

        <div className="description">
          <p>{edge.node.description}</p>
        </div>
      </div>
    ))}
  </Experiences>
);

export default ExperienceSection;
