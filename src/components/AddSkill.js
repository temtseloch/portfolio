import React, { useState, useContext } from "react";
import { navigate } from "gatsby";
import { FirebaseContext } from "./Firebase";
import { Form, Input, Button } from "./common";

const AddSkill = () => {
  const { firebase } = useContext(FirebaseContext);
  const [skillName, setSkillName] = useState("");
  const [success, setSuccess] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();

    firebase
      .createSkill({
        skillName,
      })
      .then(() => {
        setSkillName("");
        setSuccess(true);
        navigate("/");
      });
  }

  return (
    <Form
      style={{
        border: "1px solid #ddd",
        marginBottom: "50px",
        padding: "10px",
        borderRadius: "10px",
      }}
      onSubmit={handleSubmit}
    >
      <Input
        value={skillName}
        placeholder="Skill"
        onChange={(e) => {
          e.persist();
          setSuccess(false);
          setSkillName(e.target.value);
        }}
      />
      <Button type="submit" block>
        Add new skill
      </Button>
      {!!success && <span>Skill created successfully!</span>}
    </Form>
  );
};

export default AddSkill;
