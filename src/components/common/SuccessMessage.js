import styled from "styled-components";

export const SuccessMessage = styled.span`
  width: 100%;
  display: block;
  color: green;
  text-align: center;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
`;
