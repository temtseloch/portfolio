import styled from "styled-components";

export const Form = styled.form`
  max-width: 500px;
  margin: 0 auto;

  h2 {
    text-align: center;
  }
`;
